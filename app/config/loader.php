<?php

$loader = new \Phalcon\Loader();

$loader
    ->registerDirs([
        $config->application->controllersDir,
        $config->application->modelsDir
    ])
    ->registerNamespaces([
        'Phalcon' => $config->application->libraryDir,
        'Components' => $config->application->componentsDir,
    ])
    ->register();