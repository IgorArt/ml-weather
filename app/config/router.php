<?php

$router = $di->getRouter();

$router->add(
    '/api/get-weather',
    [
        'controller' => 'api',
        'action' => 'getWeather'
    ]
);

$router->add(
    '/api/get-cities',
    [
        'controller' => 'api',
        'action' => 'getCities'
    ]
);

$router->handle();