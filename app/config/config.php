<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => getenv('DB_HOST') ?: '',
        'username'    => getenv('DB_USER') ?: '',
        'password'    => getenv('DB_PASSWORD') ?: '',
        'dbname'      => getenv('DB_NAME') ?: '',
        'charset'     => 'utf8',
    ],

    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'componentsDir'  => APP_PATH . '/components/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ],

    'cache' => [
        'front' => [
            'lifetime' => 172800
        ],
        'redis' => [
            'host'       => getenv('CACHE_REDIS_HOST') ?: '', // localhost
            'port'       => getenv('CACHE_REDIS_PORT') ?: '', // 6379
            'auth'       => getenv('CACHE_REDIS_PASS') ?: '',
            'persistent' => false,
            'index'      => 0,
        ],
    ],

    'openWeatherMap' => [
        'apiKey' => getenv('OPEN_WEATHER_MAP_API_KEY') ?: '', //https://openweathermap.org/
        'baseUri' => 'api.openweathermap.org/data/2.5/',
        'cacheRepeatRequest' => 60,
        'units' => 'metric' // metric (Celsius) / imperial (Fahrenheit) or empty (Kelvin)
    ],
]);
