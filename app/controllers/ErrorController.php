<?php

class ErrorController extends ControllerBase
{
    public function show404Action()
    {
        $this->response->setStatusCode(404, 'Oops, page not found!');
        $this->view->pick('errors/404');
    }
}