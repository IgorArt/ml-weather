<?php

use Components\Weather\WeatherProvider;
use Components\City\CityManager;
use Phalcon\Http\Client\Provider\Exception as HttpClientException;

class ApiController extends ControllerBase
{
    /**
     * Get weather from request
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function getWeatherAction()
    {
        if (!$this->request->isPost()) {
            return $this->responseJson('Request don\'t made using POST', 422);
        }

        $weatherProvider = new WeatherProvider;

        $type = $this->request->get('type');

        switch ($type) {
            case 'geo':
                $latitude = $this->request->get('latitude');
                $longitude = $this->request->get('longitude');

                if (!$latitude || !$longitude) {
                    return $this->responseJson('There are no city coordinates', 422);
                }

                break;

            case 'place':
                $city = $this->request->get('city');

                if (!$city) {
                    return $this->responseJson('City is empty', 422);
                }

                break;

            default:
                return $this->responseJson('Type is undefined', 422);
        }

        try {
            if ($type === 'geo') {
                $weather = $weatherProvider->getByGeo($latitude, $longitude);
            } elseif($type === 'place') {
                $weather = $weatherProvider->getByCity($city);
            }

        } catch (HttpClientException $e) {
            return $this->responseJson($e->getMessage(), 422);
        }

        /* Save city */
        if($type === 'place') {
            $cityManager = new CityManager;
            $cityManager->addToHistory($city);
        }

        if ($this->request->isAjax()) {
            return $this->responseJson($weather);
        } else {
            return $this->response->redirect('/');
        }
    }

    public function getCitiesAction()
    {
        $cityManager = new CityManager;
        $cities = $cityManager->getAll();

        return $this->responseJson($cities);
    }
}