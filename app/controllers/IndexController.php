<?php

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->weather = $this->cache->exists('weather.response')
            ? json_decode($this->cache->get('weather.response'))
            : false;
    }
}

