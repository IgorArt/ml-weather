<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\ResponseInterface;

class ControllerBase extends Controller
{
    /**
     * Response JSON
     *
     * @param mixed $content
     * @param int $code
     * @return ResponseInterface
     */
    protected function responseJson($content, int $code = 200): ResponseInterface
    {
        $this->response->setStatusCode($code);
        $this->response->setJsonContent($content);

        return $this->response;
    }
}
