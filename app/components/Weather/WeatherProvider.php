<?php

namespace Components\Weather;

use Phalcon\Http\Client\Provider\Exception as HttpClientException;
use Phalcon\Http\Client\Request as HttpClient;
use Phalcon\Http\Client\Response as HttpClientResponse;
use Phalcon\Mvc\User\Component;

/**
 * Class WeatherProvider
 * @package Components\Weather
 */
class WeatherProvider extends Component
{
    /**
     * @var int
     */
    protected $cacheRepeatRequest = 0;

    /**
     * @var string
     */
    protected $apiKey = '';

    /**
     * @var string
     */
    protected $baseUri = '';

    /**
     * @var string
     */
    protected $units = '';

    /**
     * WeatherProvider constructor
     */
    public function __construct()
    {
        $this->cacheRepeatRequest = $this->config->openWeatherMap->cacheRepeatRequest;
        $this->apiKey = $this->config->openWeatherMap->apiKey;
        $this->baseUri = $this->config->openWeatherMap->baseUri;
        $this->units = $this->config->openWeatherMap->units;
    }

    /**
     * Get weather by city name
     *
     * @param string $city
     * @return mixed
     * @throws HttpClientException
     */
    public function getByCity(string $city)
    {
        $prevCity = $this->cache->get('weather.city');

        /* Return from cache if request is not changing */
        if ($city === $prevCity && ($response = $this->cache->get('weather.response'))) {
            return json_decode($response);
        }

        $response = $this->callApi('weather', [
            'q' => $city
        ]);

        /* Caching */
        $this->cache->save('weather.city', $city, $this->cacheRepeatRequest);
        $this->cache->save('weather.response', $response->body, $this->cacheRepeatRequest);
        $this->cache->delete('weather.geo');

        return $response->data;
    }

    /**
     * Get weather by geo
     *
     * @param string $latitude
     * @param string $longitude
     * @return mixed
     * @throws HttpClientException
     */
    public function getByGeo(string $latitude, string $longitude)
    {
        $prevGeo = $this->cache->get('weather.geo');

        /* Return from cache if request is not changing */
        if ($latitude.'/'.$longitude === $prevGeo && ($response = $this->cache->get('weather.response'))) {
            return json_decode($response);
        }

        $response = $this->callApi('weather', [
            'lat' => $latitude,
            'lon' => $longitude,
        ]);

        /* Caching */
        $this->cache->save('weather.geo', $latitude.'/'.$longitude, $this->cacheRepeatRequest);
        $this->cache->save('weather.response', $response->body, $this->cacheRepeatRequest);
        $this->cache->delete('weather.city');

        return $response->data;
    }

    /**
     * Request to API
     *
     * @param string $uri
     * @param array $params
     * @return HttpClientResponse
     * @throws HttpClientException
     */
    protected function callApi(string $uri, array $params = []): HttpClientResponse
    {
        $provider = HttpClient::getProvider();

        /* Config request */
        $provider->setBaseUri($this->baseUri);
        $provider->header->set('Accept', 'application/json');

        $params['APPID'] = $this->apiKey;

        if ($this->units) {
            $params['units'] = $this->units;
        }

        /* Request */
        $response = $provider->get($uri, $params);

        $response->data = json_decode($response->body);

        /* Check response */
        if ($response->data->cod !== 200) {
            throw new HttpClientException($response->data->message);
        }

        return $response;
    }
}