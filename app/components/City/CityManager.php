<?php

namespace Components\City;

use Cities;
use Phalcon\Mvc\User\Component;

/**
 * Class CityManager
 * @package Components\City
 */
class CityManager extends Component
{
    /**
     * @var int
     */
    protected $ttl = -1;

    /**
     * Get all cities
     *
     * @return array
     */
    public function getAll(): array
    {
        if (!$cities = $this->cache->get('cities')) {
            $cities = Cities::find([
                'columns' => 'name',
            ])->toArray();

            $cities = $this->flatCollectionArray($cities);

            $this->cache->save('cities', $cities, $this->ttl);
        }

        return $cities;
    }

    /**
     * Save city to history
     *
     * @param string $city
     */
    public function addToHistory(string $city): void
    {
        $cities = $this->cache->exists('cities') ? $this->cache->get('cities') : [];

        if (!$cities || !in_array($city, $cities, true)) {
            $newCity = new Cities;
            $newCity->name = $city;

            if ($newCity->save() !== false) {
                $cities[] = $city;
                $this->cache->save('cities', $cities, $this->ttl);
            }
        }
    }

    /**
     * Convert collection array to simple array
     *
     * @param array $array
     * @return array
     */
    protected function flatCollectionArray(array $cities): array
    {
        $result = [];

        foreach ($cities as $city) {
            $result[] = $city['name'];
        }

        return $result;
    }
}