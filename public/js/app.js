(function($, window, document, undefined){
    const app = {
        typeRows: null,
        showTypeField: value => {
            app.typeRows
                .hide()
                .filter('#' + value + '-field-row')
                .show()
        }
    }

    $(document).ready(() => {
        $.get('/api/get-cities', data => {
            let list = {}
            for (let i in data) {
                list[data[i]] = null
            }
            $('input.city-autocomplete').autocomplete({
                data: list,
            })
        })

        app.typeRows = $('.type-field-row')
        app.showTypeField($('input[type="radio"][name="type"]:checked').val())

        $('input[type="radio"][name="type"]').on('change', e => {
            app.showTypeField(e.target.value)
        })
    });
})(jQuery, window, document)