##ML Weather App

###Deploy
1. Заполните конфигурацию (в скобках ENV имена):  
Вы можете указать значения в app/config/config.php, public/.htaccess, server. Можно скопировать config.php в development/config.php
    - database.host (DB_HOST)  
    - database.username (DB_USER)  
    - database.password (DB_PASSWORD)  
    - database.dbname (DB_NAME)  
    - openWeatherMap.apiKey (OPEN_WEATHER_MAP_API_KEY) (https://openweathermap.org)
    - cache.redis.host (CACHE_REDIS_HOST)
    - cache.redis.port (CACHE_REDIS_PORT)
    - cache.redis.auth (CACHE_REDIS_PASS)

2. Запустите миграции:
```
phalcon migration run
```